#pragma once

#include <exception>
#include <stdexcept>
#include <string>

namespace raspirobot {

class RobotException : public std::exception {
private:
  std::runtime_error M;

public:
  explicit RobotException(std::string const &msg) : M(msg){};
  ~RobotException() override{};

public:
  const char *what() const noexcept override { return M.what(); }
};
} // namespace raspirobot
