#pragma once

#include <ros/ros.h>

#include <vector>

#include "controlling/simple_controller.h"
#include "controlling/simple_pin.h"

#include "raspirobot_msgs/Direction.h"
#include "raspirobot_msgs/Speed.h"

class Robot {
private:
  MotorController motorController;
  ros::ServiceServer moveService;

  ros::NodeHandle n;
  ros::NodeHandle pn;

public:
  explicit Robot(MotorController &&controller);

  bool setDirection(raspirobot_msgs::Direction::Request &req,
            raspirobot_msgs::Direction::Response &res);

  bool setSpeed(raspirobot_msgs::Speed::Request &req,
                raspirobot_msgs::Speed::Response &res);
};