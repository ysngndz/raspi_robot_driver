#pragma once

#include "controlling/simple_motor.h"
#include <memory>

class MotorController {
private:
  std::unique_ptr<Motor> m1, m2;

public:
  MotorController(std::unique_ptr<Motor> _m1, std::unique_ptr<Motor> _m2);

  void driveLeft();
  void driveRight();
  void driveForward();
  void driveBackward();
  void stop();

  bool isDrivingLeft();
  bool isDrivingForward();
  bool isDrivingRight();
  bool isDrivingBackward();
}; // class motor controller