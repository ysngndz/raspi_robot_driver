#pragma once

#include <wiringPi.h>

enum class PinType { TEST_PIN, REAL_PIN, PWM_PIN };

class Pin {
public:
  static bool wpiInitialized;

protected:
  int wpiNumber;
  bool outputPin = false;
  bool high = false;

public:
  explicit Pin(int number);

  static void init();

  virtual void setAsOutput();

  virtual void setHigh();

  virtual void setLow();

  virtual PinType getType() const = 0;

  virtual bool isHigh() const;
};

/**
 * Class that simulates real pins for tests
 */
class MockPin : public Pin {
public:
  explicit MockPin(int number);

  void setAsOutput() override;

  void setHigh() override;

  void setLow() override;

  PinType getType() const override;
};

class RaspiPin : public Pin {
public:
  explicit RaspiPin(int number);

  void setAsOutput() override;

  void setHigh() override;

  void setLow() override;

  PinType getType() const override;
};

class SoftPwmPin : public Pin {

public:
  explicit SoftPwmPin(int number, int initialValue = 0, int range = 100);

  void setHigh() override;
  void setLow() override;

  bool isHigh() const override;

  void setDutyCycle(int value);

  PinType getType() const override;

  ~SoftPwmPin();

private:
  int currentDutyCycle;
  int pwmRange;
};