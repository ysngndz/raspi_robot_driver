#pragma once
#include <map>
#include <set>

namespace gpioPins {

enum class PHYSICAL_PINS2 : int {
  PhysicalPin_11 = 0,
  PhysicalPin_12 = 1,
  PhysicalPin_13 = 2,
  PhysicalPin_15 = 3,
  PhysicalPin_16 = 4,
  PhysicalPin_18 = 5,
  PhysicalPin_22 = 6,
  PhysicalPin_07 = 7,
  PhysicalPin_03 = 8,
  PhysicalPin_05 = 9,
  PhysicalPin_24 = 10,
  PhysicalPin_26 = 11,
  PhysicalPin_19 = 12,
  PhysicalPin_21 = 13,
  PhysicalPin_23 = 14,
  PhysicalPin_08 = 15,
  PhysicalPin_10 = 16,
  PhysicalPin_29 = 21,
  PhysicalPin_31 = 22,
  PhysicalPin_33 = 23,
  PhysicalPin_35 = 24,
  PhysicalPin_37 = 25,
  PhysicalPin_32 = 26,
  PhysicalPin_36 = 27,
  PhysicalPin_38 = 28,
  PhysicalPin_40 = 29,
};

const static std::map<std::string, PHYSICAL_PINS2> RASPI_PINS{
    std::make_pair("GPIO02", PHYSICAL_PINS2::PhysicalPin_03),
    std::make_pair("GPIO03", PHYSICAL_PINS2::PhysicalPin_05),
    std::make_pair("GPIO04", PHYSICAL_PINS2::PhysicalPin_07),

    std::make_pair("GPIO17", PHYSICAL_PINS2::PhysicalPin_11),
    std::make_pair("GPIO27", PHYSICAL_PINS2::PhysicalPin_13),
    std::make_pair("GPIO22", PHYSICAL_PINS2::PhysicalPin_15),

    std::make_pair("GPIO10", PHYSICAL_PINS2::PhysicalPin_19),
    std::make_pair("GPIO09", PHYSICAL_PINS2::PhysicalPin_21),
    std::make_pair("GPIO11", PHYSICAL_PINS2::PhysicalPin_22),

    std::make_pair("GPIO14", PHYSICAL_PINS2::PhysicalPin_08),
    std::make_pair("GPIO15", PHYSICAL_PINS2::PhysicalPin_10),
    std::make_pair("GPIO18", PHYSICAL_PINS2::PhysicalPin_12),

    std::make_pair("GPIO23", PHYSICAL_PINS2::PhysicalPin_16),
    std::make_pair("GPIO24", PHYSICAL_PINS2::PhysicalPin_18),

    std::make_pair("GPIO25", PHYSICAL_PINS2::PhysicalPin_22),
    std::make_pair("GPIO08", PHYSICAL_PINS2::PhysicalPin_24),
    std::make_pair("GPIO07", PHYSICAL_PINS2::PhysicalPin_26)};

}; // namespace gpioPins
