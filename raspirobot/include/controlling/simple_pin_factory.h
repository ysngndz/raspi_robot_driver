#ifndef RASPIROBOT_SIMPLE_PIN_FACTORY_H
#define RASPIROBOT_SIMPLE_PIN_FACTORY_H

#include <memory>

template <class pinType> class PinFactory {
public:
  static std::unique_ptr<pinType> createPin(int wpiNumber) {
    return std::make_unique<pinType>(wpiNumber);
  };
};

#endif // RASPIROBOT_SIMPLE_PIN_FACTORY_H
