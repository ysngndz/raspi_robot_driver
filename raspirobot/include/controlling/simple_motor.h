#pragma once

#include "controlling/simple_pin.h"
#include <memory>

class Motor {
protected:
  std::unique_ptr<Pin> enable, a, b;

public:
  virtual void forward();
  virtual void backward();
  virtual void stop();

  virtual bool isForward();
  virtual bool isBackward();
  virtual bool isStop();

  virtual void setSpeed(int value);

  virtual void setAsOutput();

protected:
  virtual void resetPins();
  virtual void createPins(int enable_pin, int a_pin, int b_pin) = 0;
};

class ServoMotor {};

class DcMotor : public Motor {

public:
  DcMotor(int _enable, int first, int second);
  ~DcMotor();

protected:
  void createPins(int enable_pin, int a_pin, int b_pin) override;
};

class SpeedDcMotor : public Motor {
public:
  SpeedDcMotor(int _enable, int first, int second);
  ~SpeedDcMotor();

  void setSpeed(int value) override;

protected:
  void createPins(int pwmPin, int a_pin, int b_pin) override;

private:
  const int range{100};
  const int initialPwmValue{0};
};

class MockDcMotor : public Motor {
public:
  MockDcMotor(int enable, int first, int second);
  ~MockDcMotor();

private:
  void createPins(int enable_pin, int a_pin, int b_pin) override;
};