#include "controlling/simple_motor.h"

#include <gtest/gtest.h>

class MotorTest : public ::testing::Test {
protected:
  std::unique_ptr<Motor> motor;

  MotorTest() { motor = std::make_unique<MockDcMotor>(1, 2, 3); }

  ~MotorTest() override = default;

  // Called after constructor
  void SetUp() override {}

  // Called before destructor
  void TearDown() override {}
};

TEST_F(MotorTest, initPin) {
  EXPECT_TRUE(Pin::wpiInitialized);
  EXPECT_TRUE(motor->isStop());
}

TEST_F(MotorTest, forward) {
  motor->forward();
  EXPECT_TRUE(motor->isForward());
}

TEST_F(MotorTest, backward) {
  motor->backward();
  EXPECT_TRUE(motor->isBackward());
}

TEST_F(MotorTest, stop) {
  motor->stop();
  EXPECT_TRUE(motor->isStop());
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
