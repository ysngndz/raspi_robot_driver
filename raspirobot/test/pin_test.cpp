#include "controlling/simple_pin.h"

#include "memory"
#include <gtest/gtest.h>

class PinTest : public ::testing::Test {
protected:
  std::unique_ptr<Pin> pin;

  PinTest() { pin = std::make_unique<MockPin>(42); }

  ~PinTest() override = default;

  // Called after constructor
  void SetUp() override {}

  // Called before destructor
  void TearDown() override {}

};

TEST_F(PinTest, initing) {
  EXPECT_TRUE(Pin::wpiInitialized);
  EXPECT_TRUE(!pin->isHigh());
}

TEST_F(PinTest, type) {
  EXPECT_TRUE(pin->getType() == PinType::TEST_PIN);
}

TEST_F(PinTest, setHigh) {
  pin->setHigh();
  EXPECT_TRUE(pin->isHigh());
}

TEST_F(PinTest, setLow) {
  pin->setHigh();
  pin->setLow();
  EXPECT_TRUE(!pin->isHigh());
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
