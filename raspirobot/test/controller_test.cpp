#include "controlling/simple_controller.h"

#include <gtest/gtest.h>

class ControllerTest : public ::testing::Test {
protected:
  std::unique_ptr<MotorController> controller;

  ControllerTest() {
    controller = std::make_unique<MotorController>(
        std::make_unique<MockDcMotor>(1, 2, 3),
        std::make_unique<MockDcMotor>(4, 5, 6));
  }

  ~ControllerTest() override = default;

  // Called after constructor
  void SetUp() override {}

  // Called before destructor
  void TearDown() override {}
};

TEST_F(ControllerTest, initPin) {
  EXPECT_TRUE(Pin::wpiInitialized);
}

TEST_F(ControllerTest, left) {
  controller->driveLeft();
  EXPECT_TRUE(controller->isDrivingLeft());
}

TEST_F(ControllerTest, right) {
  controller->driveRight();
  EXPECT_TRUE(controller->isDrivingRight());
}

TEST_F(ControllerTest, forward) {
  controller->driveForward();
  EXPECT_TRUE(controller->isDrivingForward());
}

TEST_F(ControllerTest, backward) {
  controller->driveBackward();
  EXPECT_TRUE(controller->isDrivingBackward());
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
