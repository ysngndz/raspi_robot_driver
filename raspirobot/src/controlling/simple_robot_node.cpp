#include "controlling/simple_robot.h"
#include "ros/ros.h"

#include <memory>

class RobotNode {
private:
  std::unique_ptr<Robot> raspiRobot;

public:
  RobotNode() {
    raspiRobot = std::make_unique<Robot>(
        MotorController(std::make_unique<DcMotor>(0, 2, 3),
                        std::make_unique<DcMotor>(1, 4, 5)));
  }
};

int main(int argc, char **argv) {
  ros::init(argc, argv, "simple_robot");
  RobotNode node;
  ros::spin();
  return 0;
}