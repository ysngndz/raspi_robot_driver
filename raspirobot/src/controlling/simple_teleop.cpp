#include "geometry_msgs/Twist.h"
#include "raspirobot_msgs/Direction.h"
#include "ros/ros.h"

#include <chrono>

constexpr std::chrono::seconds timeout{2};

class TwistListener {
private:
  ros::NodeHandle n;
  ros::Subscriber twistSubscriber;
  ros::ServiceClient client;
  std::chrono::milliseconds lastCallback;

  void velCallback(geometry_msgs::Twist::ConstPtr const &msg) {
    raspirobot_msgs::Direction srv;
    if (msg->angular.z > 0.60) {
      srv.request.direction = "Left";
    } else if (msg->angular.z < -0.60) {
      srv.request.direction = "Right";
    } else {
      if (msg->linear.x > 0) {
        srv.request.direction = "Forward";
      } else if (msg->linear.x < 0) {
        srv.request.direction = "Backward";
      } else {
        srv.request.direction = "Stop";
      }
    }
    srv.request.direction = "Forward";
    if (!client.call(srv)) {
      ROS_ERROR("Could not reach the robot to go");
    }
    ROS_INFO(std::string(srv.request.direction).c_str());
  }

public:
  TwistListener() {
    n = ros::NodeHandle();
    twistSubscriber = n.subscribe<geometry_msgs::Twist>(
        "/cmd_vel", 1000, &TwistListener::velCallback, this);
    client = n.serviceClient<raspirobot_msgs::Direction>("/robot_direction");
  }
};

int main(int argc, char **argv) {
  ros::init(argc, argv, "twist_listener");

  TwistListener robotClient;

  ros::spin();

  return 0;
}