#include "controlling/simple_motor.h"
#include "controlling/simple_pin.h"

DcMotor::DcMotor(int _enable, int first, int second) {
  DcMotor::createPins(_enable, first, second);
  Motor::setAsOutput();
}

void Motor::forward() {
  enable->setHigh();
  a->setHigh();
  b->setLow();
}
void Motor::backward() {
  enable->setHigh();
  a->setLow();
  b->setHigh();
}

void Motor::stop() { resetPins(); }

void Motor::resetPins() {
  enable->setLow();
  a->setLow();
  b->setLow();
}

void DcMotor::createPins(int enable_pin, int a_pin, int b_pin) {
  enable = std::make_unique<RaspiPin>(enable_pin);
  a = std::make_unique<RaspiPin>(a_pin);
  b = std::make_unique<RaspiPin>(b_pin);
}
DcMotor::~DcMotor() { Motor::stop(); }

bool Motor::isForward() {
  return enable->isHigh() && a->isHigh() && !b->isHigh();
}

bool Motor::isBackward() {
  return enable->isHigh() && !a->isHigh() && b->isHigh();
}

bool Motor::isStop() { return !a->isHigh() && !b->isHigh(); }

void Motor::setAsOutput() {
  enable->setAsOutput();
  a->setAsOutput();
  b->setAsOutput();
}

void Motor::setSpeed(int value) {}

void MockDcMotor::createPins(int enable_pin, int a_pin, int b_pin) {
  enable = std::make_unique<MockPin>(enable_pin);
  a = std::make_unique<MockPin>(a_pin);
  b = std::make_unique<MockPin>(b_pin);
}

MockDcMotor::MockDcMotor(int _enable, int first, int second) {
  MockDcMotor::createPins(_enable, first, second);
  Motor::setAsOutput();
}

MockDcMotor::~MockDcMotor() { Motor::stop(); }

SpeedDcMotor::SpeedDcMotor(int _enable, int first, int second) {
  SpeedDcMotor::createPins(_enable, first, second);
  Motor::setAsOutput();
}

SpeedDcMotor::~SpeedDcMotor() { Motor::stop(); }

void SpeedDcMotor::createPins(int pwmPin, int a_pin, int b_pin) {
  enable = std::make_unique<SoftPwmPin>(pwmPin, SpeedDcMotor::initialPwmValue,
                                        SpeedDcMotor::range);
  a = std::make_unique<RaspiPin>(a_pin);
  b = std::make_unique<RaspiPin>(b_pin);
}

void SpeedDcMotor::setSpeed(int value) {
  auto enable_pin = dynamic_cast<SoftPwmPin *>(enable.get());
  if (enable_pin) {
    enable_pin->setDutyCycle(value);
  }
}
