#include "controlling/simple_controller.h"

MotorController::MotorController(std::unique_ptr<Motor> _m1,
                                 std::unique_ptr<Motor> _m2) {
  m1 = std::move(_m1);
  m2 = std::move(_m2);
}

void MotorController::driveLeft() {
  m1->forward();
  m2->backward();
}

void MotorController::driveRight() {
  m1->backward();
  m2->forward();
}

void MotorController::driveForward() {
  m1->forward();
  m2->forward();
}

void MotorController::driveBackward() {
  m1->backward();
  m2->backward();
}

void MotorController::stop() {
  m1->stop();
  m2->stop();
}

bool MotorController::isDrivingLeft() {
  return m1->isForward() && m2->isBackward();
}

bool MotorController::isDrivingForward() {
  return m1->isForward() && m2->isForward();
}

bool MotorController::isDrivingRight() {
  return m1->isBackward() && m2->isForward();
}

bool MotorController::isDrivingBackward() {
  return m1->isBackward() && m2->isBackward();
}
