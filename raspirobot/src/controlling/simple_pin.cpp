#include "controlling/simple_pin.h"

#include "controlling/robotException.h"

#include <softPwm.h>
#include <wiringPi.h>

bool Pin::wpiInitialized = false;

Pin::Pin(int number) : wpiNumber(number), outputPin(false), high(false) {}

void Pin::init() { wiringPiSetup(); }

void Pin::setAsOutput() { outputPin = true; }

void Pin::setHigh() { high = true; }

void Pin::setLow() { high = false; }

bool Pin::isHigh() const { return high; }

RaspiPin::RaspiPin(int number) : Pin(number) {
  if (!Pin::wpiInitialized) {
    wiringPiSetup();
    Pin::wpiInitialized = true;
  }
}

void RaspiPin::setAsOutput() {
  Pin::setAsOutput();
  pinMode(wpiNumber, OUTPUT);
}

void RaspiPin::setHigh() {
  Pin::setHigh();
  digitalWrite(wpiNumber, HIGH);
}

void RaspiPin::setLow() {
  Pin::setLow();
  digitalWrite(wpiNumber, LOW);
}

PinType RaspiPin::getType() const { return PinType::REAL_PIN; }

MockPin::MockPin(int number) : Pin(number) {
  if (!Pin::wpiInitialized) {
    Pin::wpiInitialized = true;
  }
}

void MockPin::setAsOutput() { Pin::setAsOutput(); }

void MockPin::setHigh() { Pin::setHigh(); }

void MockPin::setLow() { Pin::setLow(); }

PinType MockPin::getType() const { return PinType::TEST_PIN; }

PinType SoftPwmPin::getType() const { return PinType::PWM_PIN; }

SoftPwmPin::SoftPwmPin(int number, int initialValue, int range)
    : Pin(number), currentDutyCycle(initialValue), pwmRange(range) {
  int result = softPwmCreate(number, initialValue, range);
  if (result != 0) {
    throw raspirobot::RobotException("Could not initialize pwm pin " +
                                     std::to_string(number));
  }
  // pwm pin is always on output;
  outputPin = true;
}

void SoftPwmPin::setDutyCycle(int value) {
  if (value < 0 || value > pwmRange) {
    throw raspirobot::RobotException("Cannot set pwm value. Either value is "
                                     "not valid or is out of range. value: " +
                                     std::to_string(value));
  }
  currentDutyCycle = value;
  softPwmWrite(wpiNumber, currentDutyCycle);
}

SoftPwmPin::~SoftPwmPin() { softPwmStop(wpiNumber); }

void SoftPwmPin::setHigh() {
  Pin::setHigh();
  softPwmWrite(wpiNumber, currentDutyCycle);
}

void SoftPwmPin::setLow() {
  Pin::setLow();
  softPwmWrite(wpiNumber, 0);
}

bool SoftPwmPin::isHigh() const { return currentDutyCycle > 0; }
