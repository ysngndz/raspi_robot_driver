#include <ros/ros.h>

#include <controlling/simple_robot.h>
#include <vector>

#include "controlling/simple_controller.h"
#include "controlling/simple_pin.h"

#include "raspirobot_msgs/Direction.h"
#include "raspirobot_msgs/Speed.h"

Robot::Robot(MotorController &&controller)
    : motorController(std::move(controller)) {

  n = ros::NodeHandle();
  pn = ros::NodeHandle("~");

  moveService = n.advertiseService("robot_direction", &Robot::setDirection, this);
};

bool Robot::setDirection(raspirobot_msgs::Direction::Request &req,
                 raspirobot_msgs::Direction::Response &res) {
  if (req.direction == "Forward") {
    motorController.driveForward();
  } else if (req.direction == "Backward") {
    motorController.driveBackward();
  } else if (req.direction == "Left") {
    motorController.driveLeft();
  } else if (req.direction == "Right") {
    motorController.driveRight();
  } else if (req.direction == "Stop") {
    motorController.stop();
  } else {
    std::string error = "Direction is not defined: " + req.direction;
    ROS_ERROR("%s", error.c_str());
    res.error = error;
    return false;
  }
  res.error = std::string();
  return true;
}

bool Robot::setSpeed(raspirobot_msgs::Speed::Request &req,
                     raspirobot_msgs::Speed::Response &res) {
  return true;
}
