# Setup ROS on Raspberry Pi3 (Model B+)

A Guide for setting up ros for raspberry pi 3.

## Setup sd-card

Get ubuntu mate 16 [here](https://ubuntu-mate.org/download/) and download the pi version.
Afterwards do the following to the sd card (Check mount-name with lsblk - in my case /dev/mmcblk0). Also write the
image file to the disk and not on the part.

```bash
sudo apt-get install gddrescue xz-utils
unxz ubuntu-mate-16.04.2-desktop-armhf-raspberry-pi.img.xz
sudo ddrescue -D --force ubuntu-mate-16.04.2-desktop-armhf-raspberry-pi.img /dev/sdx
```

## Setup pi prior and after first boot 

Go to the raspi-config and enable ssh. Or go into the /boot partition and create an empty folder
named ssh. 

## Install ROS

Get the ros distro [here](http://wiki.ros.org/ROSberryPi/Installing%20ROS%20Kinetic%20on%20the%20Raspberry%20Pi)
Just do the following steps
Setup ROS-Repo:
```bash
$ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
$ sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
$ sudo apt-get update
$ sudo apt-get install ros-melodic-ros-base
```

Install dependencies for building ros paclages
```bash
$ sudo apt install python-rosinstall python-rosinstall-generator python-wstool build-essential
```

Init rosdep
```bash
$ sudo rosdep init
$ rosdep update
```

Environment setup
```bash
$ echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
$ source ~/.bashrc
```

Workspace setup
```bash
$ mkdir workspace
$ cd workspace/
$ mkdir src
$ cd .. && catkin_make
$ echo "source .../workspace/devel/setup.bash" >> ~/.bashrc

```
# SSHing
enable ssh ob the raspberry pi. Go into raspi-config and enbale ssh. If not, you have to enable ssh via
```bash
$ sudo systemctl enable ssh
$ /etc/init.d/ssh start
```
on the pi. After this, look that your pi and the laptop are in the same subnet. The command to connect to you pi is:
```bash
$ ssh username@static-ip
```
For ubuntu mate 18.04 version, the ssh keys are all empty. That is why you have to add generate them manually
```bash
sudo ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
sudo ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key
sudo ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key
```
at the pi.
# Running Ros on multiple machines

On the master machine (e.g. controller/laptop/etc.)
```bash
export ROS_MASTER_URI=http://192.168.123.46:11311
export ROS_IP=192.168.123.46 (master ip)
```

On the client machine .45 (e.g. raspi pi)
```bash
export ROS_MASTER_URI=http://192.168.123.46:11311
export ROS_IP=192.168.123.45 (client ip)
```

Go into one machine and start roscore
take the ROS_MASTER_URI = XYZ
and go to the other machine and export ROS_MASTER_URI = XYZ